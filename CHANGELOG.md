# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.2] - 2022-02-08
### Changed
- Project url
- Bump dependencies

## [0.1.1] - 2021-10-01
### Added
- Missing dependecy for pnglitch

## [0.1.0] - 2021-08-06
### Added
- Progressive pixel sorting

### Fixed
- Sampler output filename attribute

### Added
- Slim direction now support vertical, vertical_inverted, horizontal and horizontal_inverted

## [0.0.9] - 2021-08-06
### Fixed
- Pixel sorted sort by attribute
- Sampler output filename attribute

## [0.0.8] - 2021-07-01
### Added
- Icon

### Changed
- Url of the project
- In sampler use Io output_filename instead of base_output_filename

## [0.0.7] - 2021-06-06
### Changed
- Privatise PNG_EXTENSIONS, JPG_EXTENSIONS and Paths in Io

### Fixed
- Transpose algorithm
- Include missing required files in glitcher

### Removed
- Spec files from the gem

## [0.0.6] - 2021-05-28
### Added
- Add Sampler in the main lib

## [0.0.5] - 2021-05-28
### Added
- Rusty Engine dependency in gemspec file

## [0.0.4] - 2021-05-28
### Added
- Acknowledgements section in README

### Changed
- The following Sampler constant are not publically available anymore : FILTERS, SLIM_DIRECTION and BRUSH_DIRECTION

### Fixed
- By default the Sampler now also run the Brush algorithm

## [0.0.3] - 2021-05-26
### Changed
- Replaces Rusty Engine library with the gem encapsulating those files
