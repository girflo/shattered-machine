# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ShatteredMachine::ChangeByte do
  describe '#call' do
    let(:output_file) { 'spec/images/change_byte.png' }
    let(:input_file) { 'spec/images/foo.png' }
    let(:io) do
      ShatteredMachine::Io.new(input_file, 'spec/images', 'change_byte')
    end
    let(:in_img) { io.png_images.first.input }
    let(:out_img) { io.png_images.first.output }
    subject { ShatteredMachine::ChangeByte.new.call(in_img, out_img) }

    after do
      File.delete(output_file) if File.exist?(output_file)
    end

    it 'change byte on image' do
      subject
      expect(File.exist?(output_file)).to be true
    end
  end
end
