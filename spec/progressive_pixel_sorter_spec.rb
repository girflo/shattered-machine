# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ShatteredMachine::ProgressivePixelSorter do
  describe '#call' do
    let(:created_files) { %w[progressive_sorter_bar.png progressive_sorter_foo.png] }
    let(:io) { ShatteredMachine::Io.new('spec/images', 'spec/images', 'progressive_sorter_') }
    let(:options) do
      { direction: :vertical, smart_sorting: true, detection_type: :lightness_range,
        detection_min: '0', detection_max: '20', detection_min_two: '0', detection_max_two: '100',
        sorting_by: :hue }
    end
    subject { ShatteredMachine::ProgressivePixelSorter.new(io, options).call }

    def output_image_full_path(file)
      "spec/images/#{file}"
    end

    after do
      created_files.each do |file|
        File.delete(output_image_full_path(file)) if File.exist?(output_image_full_path(file))
      end
    end

    it 'call sampler' do
      subject
      created_files.each do |file|
        expect(File.exist?(output_image_full_path(file))).to be true
      end
    end
  end
end
