# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ShatteredMachine::Io do
  describe '#png_images' do
    subject do
      ShatteredMachine::Io.new(input_path, 'spec/images', output_filename).png_images
    end

    context 'input path is a directory' do
      let(:input_path) { 'spec/images' }
      let(:output_filename) { 'pif' }

      it 'find two png images' do
        expect(subject.count).to eq(2)
        expect(subject.first.input).to eq('spec/images/foo.png')
        expect(subject.first.output).to eq('spec/images/piffoo.png')
      end
    end

    context 'input path is an image' do
      context 'output path is available' do
        let(:input_path) { 'spec/images/foo.png' }
        let(:output_filename) { 'paf' }

        it 'find the correct png image' do
          expect(subject.count).to eq(1)
          expect(subject.first.input).to eq('spec/images/foo.png')
          expect(subject.first.output).to eq('spec/images/paf.png')
        end
      end

      context 'output path is not available' do
        let(:input_path) { 'spec/images/foo.png' }
        let(:output_filename) { 'foo' }

        it 'find the correct png image' do
          expect(subject.count).to eq(1)
          expect(subject.first.output).to match %r(spec/images/foo_(\d{8})_(\d{6}).png)
        end
      end
    end
  end

  describe '#jpg_images' do
    subject do
      ShatteredMachine::Io.new(input_path, 'spec/images', output_filename).jpg_images
    end

    context 'input path is a directory' do
      let(:input_path) { 'spec/images' }
      let(:output_filename) { 'pouf' }

      it 'find four jpg images' do
        expect(subject.count).to eq(4)
        expect(subject.first.input).to eq('spec/images/fuzz.JPG')
        expect(subject.first.output).to eq('spec/images/pouffuzz.png')
      end
    end

    context 'input path is an image' do
      let(:input_path) { 'spec/images/foo.jpg' }
      let(:output_filename) { 'pouet' }

      it 'find the correct jpg image' do
        expect(subject.count).to eq(1)
        expect(subject.first.input).to eq('spec/images/foo.jpg')
        expect(subject.first.output).to eq('spec/images/pouet.png')
      end
    end
  end
end
