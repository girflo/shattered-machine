# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ShatteredMachine::Converter do
  describe '#call' do
    let(:converted_file) { 'spec/images/converted.png' }
    let(:input_file) { 'spec/images/foo.jpg' }
    let(:io) do
      ShatteredMachine::Io.new(input_file, 'spec/images', 'converted')
    end
    subject { ShatteredMachine::Converter.new(io).call }

    after do
      File.delete(converted_file) if File.exist?(converted_file)
    end

    it 'convert image' do
      subject
      expect(File.exist?(converted_file)).to be true
    end
  end
end
