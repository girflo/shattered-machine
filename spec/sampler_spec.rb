# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ShatteredMachine::Sampler do
  describe 'ALL_ALGORITHMS' do
    subject { ShatteredMachine::Sampler::ALL_ALGORITHMS }
    it 'returns all algorithm available in sampler' do
      expect(subject).to eq ['brush', 'change_byte', 'defect', 'exchange', 'slim', 'transpose', 'wrong_filter']
    end
  end

  describe '#call' do
    let(:created_files) do
      %w[sample_exchange_average.png sample_exchange_up.png sample_slim_vertical.png sample_transpose_sub.png
         sample_wrong_filter_paeth.png sample_exchange_none.png sample_slim_vertical_inverted.png
         sample_transpose_average.png sample_transpose_up.png sample_wrong_filter_sub.png sample_change_byte.png
         sample_exchange_paeth.png sample_slim_horizontal.png sample_transpose_none.png
         sample_wrong_filter_average.png sample_wrong_filter_up.png sample_defect.png sample_exchange_sub.png
         sample_slim_horizontal_inverted.png sample_transpose_paeth.png sample_wrong_filter_none.png
         sample_brush_horizontal_inverted.png sample_brush_horizontal.png sample_brush_vertical_inverted.png
         sample_brush_vertical.png]
    end
    let(:output_file) { 'spec/images/sample.png' }
    let(:input_file) { 'spec/images/foo.png' }
    let(:io) { ShatteredMachine::Io.new(input_file, 'spec/images', 'sample') }
    subject { ShatteredMachine::Sampler.new(io).call }

    def output_image_full_path(file)
      "spec/images/#{file}"
    end

    after do
      created_files.each do |file|
        File.delete(output_image_full_path(file)) if File.exist?(output_image_full_path(file))
      end
    end

    it 'call sampler' do
      subject
      created_files.each do |file|
        expect(File.exist?(output_image_full_path(file))).to be true
      end
    end
  end
end
