# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ShatteredMachine::Glitcher do
  describe '#call' do
    let(:output_file) { 'spec/images/glitch.png' }
    let(:input_file) { 'spec/images/foo.png' }
    let(:io) do
      ShatteredMachine::Io.new(input_file, 'spec/images', 'glitch')
    end
    subject do
      ShatteredMachine::Glitcher.new('Slim', io).call
    end

    after do
      File.delete(output_file) if File.exist?(output_file)
    end

    it 'slim image' do
      subject
      expect(File.exist?(output_file)).to be true
    end
  end
end
