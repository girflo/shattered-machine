# frozen_string_literal: true

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  $LOAD_PATH.unshift File.expand_path('..', __dir__)
  require 'lib/shattered_machine/converter'
  require 'lib/shattered_machine/brush'
  require 'lib/shattered_machine/change_byte'
  require 'lib/shattered_machine/defect'
  require 'lib/shattered_machine/exchange'
  require 'lib/shattered_machine/io'
  require 'lib/shattered_machine/glitcher'
  require 'lib/shattered_machine/pixel_sorter'
  require 'lib/shattered_machine/progressive_pixel_sorter'
  require 'lib/shattered_machine/sampler'
  require 'lib/shattered_machine/slim'
  require 'lib/shattered_machine/transpose'
  require 'lib/shattered_machine/wrong_filter'
end
