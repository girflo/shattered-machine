# frozen_string_literal: true

require 'pnglitch'
module ShatteredMachine
  # Use the transpose algorithm from pnglitch on a given png image.
  class Transpose
    ALL_FILTERS = %w[none sub up average paeth].freeze

    # @param options [Hash] options for transpose algorithm
    def initialize(options = {})
      @filter = define_filter(options[:filter]) || 'average'
      @transpose_force = options[:transpose_force] || 'half'
    end

    # @param input_image [string] path for image
    # @param output_image [string] path for output transposed image
    # @return [boolean] status of transpose
    def call(input_image, output_image)
      PNGlitch.open(input_image) do |png|
        filtered_glitch(png, @filter).save output_image
      end
      output_image
    end

    private

    def define_filter(filter_from_options)
      return filter_from_options unless filter_from_options == 'random'

      ALL_FILTERS[rand(5)]
    end

    def filtered_glitch(png, custom_filter)
      png.each_scanline do |scanline|
        scanline.change_filter custom_filter
      end
      png.glitch do |data|
        transpose_data(data)
      end
    end

    def transpose_data(data)
      quarter_image_size = data.size / 4
      return half_transpose(data, quarter_image_size) if @transpose_force == 'half'

      full_transpose(data, quarter_image_size)
    end

    def half_transpose(data, qis)
      data[0, qis] + data[qis * 2, qis] + data[qis * 1, qis] + data[qis * 3..-1]
    end

    def full_transpose(data, qis)
      data[qis * 2, qis] + data[0, qis] + data[qis * 3..-1] + data[qis * 1, qis]
    end
  end
end
