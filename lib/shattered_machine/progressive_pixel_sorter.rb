# frozen_string_literal: true

require_relative 'pixel_sorter'
module ShatteredMachine
  # Call the pixel sorter multiple time in steps for given png image(s)
  class ProgressivePixelSorter
    # @param io [ShatteredMachine::Io] Io containing paths for images to glitch
    # @param options [Hash] options for the pixel sorting algorithm
    def initialize(io, options)
      @io = io
      @options = options
      @images_count = @io.png_images.count
      @steps = define_steps
    end

    def call
      @io.png_images.sort_by { |path| [path.input[/\d+/].to_i, path.input] }.each_with_index do |item, index|
        PixelSorter.new(sorter_options(index)).call(item.input, item.output)
      end
    end

    private

    def sorter_options(index)
      return @options unless @options[:smart_sorting]

      step_options = @options.clone
      step_options[:multiple_ranges] = false
      step_options[:detection_min] = (define_new_min_limit(index)).round.to_s
      step_options[:detection_max] = (define_new_max_limit(index)).round.to_s
      step_options
    end

    def define_steps
      { min: min_step, max: max_step }
    end

    def min_step
      positive_subtraction(@options[:detection_min].to_f, @options[:detection_min_two].to_f) / (@images_count - 1)
    end

    def max_step
      positive_subtraction(@options[:detection_max].to_f, @options[:detection_max_two].to_f) / (@images_count - 1)
    end

    def positive_subtraction(value1, value2)
      value1 > value2 ? value1 - value2 : value2 - value1
    end

    def define_new_min_limit(index)
      if @options[:detection_min].to_f > @options[:detection_min_two].to_f
        (@options[:detection_min].to_f - (index * @steps[:min]))
      else
        (@options[:detection_min].to_f + (index * @steps[:min]))
      end
    end

    def define_new_max_limit(index)
      if @options[:detection_max].to_f > @options[:detection_max_two].to_f
        (@options[:detection_max].to_f - (index * @steps[:max]))
      else
        (@options[:detection_max].to_f + (index * @steps[:max]))
      end
    end
  end
end
