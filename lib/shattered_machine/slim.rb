# frozen_string_literal: true

require 'rusty_engine'

module ShatteredMachine
  # Repeat pixels of a given png image.
  # The logic for the pixel sorter come from the Rusty Engine.
  class Slim
    ALL_COLORS = %i[white black grey red green blue cyan yellow magenta].freeze

    # @param options [Hash] options for slim algorithm
    def initialize(options = {})
      @colors = options[:colors] || ALL_COLORS
      @direction = options[:direction] || :vertical_inverted
      @probability = (options[:probability] || 95).to_s
      @probability_area = options[:probability_area] || 'global'
      @colors_with_proba = colors_with_proba(options[:colors_with_proba])
    end

    # @param input_image [string] path for image
    # @param output_image [string] path for output slimed image
    # @return [boolean] status of slim
    def call(input_image, output_image)
      RustyEngine.slim(input_image, output_image, @probability, @probability_area,
                       rust_formatted_direction, rust_formatted_colors,
                       rust_formatted_color_with_proba)
    end

    private

    def colors_with_proba(colors_with_proba_options)
      if colors_with_proba_options.nil? || colors_with_proba_options.empty?
        ALL_COLORS.map { |c| [c.to_sym, @probability] }.to_h
      else
        colors_with_proba_options
      end
    end

    def rust_formatted_direction
      ruby_to_rust_directions = { up_to_down: '1', down_to_up: '2',
                                  left_to_right: '3', right_to_left: '4',
                                  vertical: '1', vertical_inverted: '2',
                                  horizontal: '3', horizontal_inverted: '4' }
      ruby_to_rust_directions[@direction]
    end

    def rust_formatted_colors
      @colors.join(',')
    end

    def rust_formatted_color_with_proba
      @colors_with_proba.map do |key, value|
        "#{key}:#{value}"
      end.join(',')
    end
  end
end
