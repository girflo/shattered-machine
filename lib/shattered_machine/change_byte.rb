# frozen_string_literal: true

require 'pnglitch'
module ShatteredMachine
  # Use the change byte algorithm from pnglitch on a given png image.
  class ChangeByte
    # @param options [Hash] options for change byte algorithm
    def initialize(options = {})
      @byte_numbers = options[:byte_numbers].to_i || 0
    end

    # @param input_image [string] path for image
    # @param output_image [string] path for output changed byte image
    # @return [boolean] status of change byte
    def call(input_image, output_image)
      PNGlitch.open(input_image) do |png|
        filtered_glitch(png).save output_image
      end
      output_image
    end

    private

    def filtered_glitch(png)
      png.each_scanline do |scanline|
        change_byte(scanline)
      end
    end

    def change_byte(scanline)
      scanline.register_filter_encoder do |data, prev|
        data.size.times.reverse_each do |i|
          x = data.getbyte(i)
          v = prev ? prev.getbyte(i - @byte_numbers) : 0
          data.setbyte(i, (x - v) & 0xff)
        end
        data
      end
    end
  end
end
