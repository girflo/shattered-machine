# frozen_string_literal: true

require 'rusty_engine'

module ShatteredMachine
  # Convert jpg image in png image
  class Converter
    # @param io [ShatteredMachine::Io] Io containing paths for images to convert
    def initialize(io)
      @io = io
    end

    # @return [boolean] status of conversion
    def call
      @io.jpg_images.each do |jpg|
        RustyEngine.convert(jpg.input, jpg.output)
      end
    end
  end
end
