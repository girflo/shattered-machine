# frozen_string_literal: true

require 'pnglitch'
module ShatteredMachine
  # A simple weay ro run one, many or all glitch algotirhm on one specific image.
  # This create a quick overview of the effect of each algo for the given image.
  class Sampler
    ALL_ALGORITHMS = %w[brush change_byte defect exchange slim transpose wrong_filter].freeze

    # @param io [ShatteredMachine::Io] Io containing paths for images to sample
    # @param options [Hash] options for specifying which
    def initialize(io, options = {})
      @io = io
      @base_output_filename = io.output_filename.empty? ? 'sample' : io.output_filename
      @algorithms_to_sample = options[:algorithms_to_sample] || ALL_ALGORITHMS
    end

    def call
      FILTERS.each do |filter|
        sample_exchange(filter)
        sample_transpose(filter)
        sample_wrong_filter(filter)
      end
      sample_slim
      sample_brush
      sample_change_byte
      sample_defect
    end

    private

    FILTERS = %w[none sub up average paeth].freeze
    DIRECTION = %w[vertical vertical_inverted horizontal horizontal_inverted].freeze

    def update_io(output_filename_appendix)
      @io.output_filename = "#{@base_output_filename}_#{output_filename_appendix}"
    end

    def sample_exchange(filter)
      return unless @algorithms_to_sample.include? 'exchange'

      exchange_options = { filter: filter }
      update_io("exchange_#{filter}")
      Glitcher.new('Exchange', @io, exchange_options).call
    end

    def sample_transpose(filter)
      return unless @algorithms_to_sample.include? 'transpose'

      transpose_options = { filter: filter }
      update_io("transpose_#{filter}")
      Glitcher.new('Transpose', @io, transpose_options).call
    end

    def sample_wrong_filter(filter)
      return unless @algorithms_to_sample.include? 'wrong_filter'

      wrong_filter_options = { algorithm: 'wrong_filter',
                               filter: filter }
      update_io("wrong_filter_#{filter}")
      Glitcher.new('WrongFilter', @io, wrong_filter_options).call
    end

    def sample_slim
      return unless @algorithms_to_sample.include? 'slim'

      DIRECTION.each do |direction|
        update_io("slim_#{direction}")
        slim_options = { direction: direction.to_sym }
        Glitcher.new('Slim', @io, slim_options).call
      end
    end

    def sample_brush
      return unless @algorithms_to_sample.include? 'brush'

      DIRECTION.each do |direction|
        update_io("brush_#{direction}")
        brush_options = { direction: direction.to_sym }
        Glitcher.new('Brush', @io, brush_options).call
      end
    end

    def sample_change_byte
      return unless @algorithms_to_sample.include? 'change_byte'

      update_io('change_byte')
      change_byte_options = { algorithm: 'change_byte' }
      Glitcher.new('ChangeByte', @io, change_byte_options).call
    end

    def sample_defect
      return unless @algorithms_to_sample.include? 'defect'

      update_io('defect')
      defect_options = { random: true, iterations: 10 }
      Glitcher.new('Defect', @io, defect_options).call
    end
  end
end
