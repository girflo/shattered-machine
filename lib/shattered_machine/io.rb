# frozen_string_literal: true

module ShatteredMachine
  # Find all png or jpg image in a given directory.
  # Generate output filename that doesn't overwrite existing file.
  class Io
    attr_accessor :output_filename

    # @param input_path [string] input file or directory
    # @param output_folder [string] output directory
    # @param output_filename [string] output file name
    def initialize(input_path, output_folder, output_filename)
      @input_path = input_path
      @output_folder = output_folder
      @output_filename = output_filename
    end

    # @return [Array<Paths>] list of all png images in input_path
    def png_images
      return single_image_io if extension_included?(PNG_EXTENSIONS, @input_path)

      images_in_directory(PNG_EXTENSIONS).map do |img|
        Paths.new("#{@input_path}/#{img}", generate_output_filename(img))
      end
    end

    # @return [Array<Paths>] list of all jpg images in input_path
    def jpg_images
      return single_image_io if extension_included?(JPG_EXTENSIONS, @input_path)

      images_in_directory(JPG_EXTENSIONS).map do |img|
        Paths.new("#{@input_path}/#{img}", generate_output_filename(img))
      end
    end

    private

    PNG_EXTENSIONS = ['.png', '.PNG'].freeze
    JPG_EXTENSIONS = ['.jpg', '.jpeg', '.JPG', '.JPEG'].freeze
    Paths = Struct.new(:input, :output)

    def single_image_io
      [Paths.new(@input_path, generate_output_filename)]
    end

    def generate_output_filename(img = '')
      img_name = File.basename(img, File.extname(img))
      if File.file?("#{@output_folder}/#{@output_filename}#{img_name}.png")
        "#{@output_folder}/#{@output_filename}#{img_name}_#{time_utc_string}.png"
      else
        "#{@output_folder}/#{@output_filename}#{img_name}.png"
      end
    end

    def time_utc_string
      t = Time.now.getutc
      t.strftime '%Y%m%d_%H%M%S'
    end

    def extension_included?(extensions, file)
      extensions.include? File.extname(file)
    end

    def images_in_directory(extensions)
      children = []
      Dir.foreach(@input_path) do |file|
        next unless extension_included?(extensions, file)

        children << file
      end
      children
    end
  end
end
