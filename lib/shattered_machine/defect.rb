# frozen_string_literal: true

require 'pnglitch'
module ShatteredMachine
  # Use the defect algorithm from pnglitch on a given png image.
  class Defect
    # @param options [Hash] options for defect algorithm
    def initialize(options = {})
      @random = options[:random] || false
      @iterations = options[:iterations] || 1
    end

    # @param input_image [string] path for input image
    # @param output_image [string] path for output defected image
    # @return [boolean] status of defect
    def call(input_image, output_image)
      PNGlitch.open(input_image) do |png|
        filtered_glitch(png).save output_image
      end
      output_image
    end

    private

    def filtered_glitch(png)
      png.glitch do |data|
        if !@random
          data.gsub(/\d/, '')
        else
          @iterations.times do
            data[rand(data.size)] = ''
          end
          data
        end
      end
    end
  end
end
