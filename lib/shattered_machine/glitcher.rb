# frozen_string_literal: true

require_relative 'brush'
require_relative 'change_byte'
require_relative 'defect'
require_relative 'exchange'
require_relative 'pixel_sorter'
require_relative 'slim'
require_relative 'transpose'
require_relative 'wrong_filter'
module ShatteredMachine
  # Main class to call from glitching image.
  class Glitcher
    # @param glitch_library [string] Name of the library to call to glitch image
    # @param io [ShatteredMachine::Io] Io containing paths for images to glitch
    # @param options [Hash] options for the glitch algorithm
    def initialize(glitch_library, io, options = {})
      @glitch_library = glitch_library
      @io = io
      @options = options
    end

    # @return [boolean] status of glitching
    def call
      @io.png_images.each do |item|
        create_glitch(item.input, item.output)
      end
    end

    private

    def glitch_library_name
      "ShatteredMachine::#{@glitch_library}"
    end

    def create_glitch(input_path, output_filename)
      Object.const_get(glitch_library_name)
            .new(@options)
            .call(input_path, output_filename)
      return true
    end
  end
end
