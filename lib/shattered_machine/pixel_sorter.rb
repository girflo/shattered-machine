# frozen_string_literal: true

require 'rusty_engine'

module ShatteredMachine
  # Sort pixels of a given png image.
  # The logic for the pixel sorter come from the Rusty Engine.
  class PixelSorter
    # @param options [Hash] options for pixel sort algorithm
    def initialize(options = {})
      @direction = options[:direction] || :vertical
      @smart_sorting = (options[:smart_sorting] || true).to_s
      @detection_type = options[:detection_type] || :lightness_range
      @detection_min = options[:detection_min] || '45'
      @detection_max = options[:detection_max] || '60'
      @multiple_ranges = (options[:multiple_ranges] || false).to_s
      @detection_min_two = options[:detection_min_two] || '75'
      @detection_max_two = options[:detection_max_two] || '90'
      @sorting_by = options[:sorting_by] || :hue
    end

    # @param input_image [string] path for image
    # @param output_image [string] path for output pixel sorted image
    # @return [boolean] status of pixel sort
    def call(input_image, output_image)
      RustyEngine.sort(input_image, output_image, rust_formatted_direction, @smart_sorting,
                       rust_formatted_detection_type, @detection_min, @detection_max, @multiple_ranges,
                       @detection_min_two, @detection_max_two, rust_formatted_sorting_by)
    end

    private

    def rust_formatted_direction
      ruby_to_rust_directions = { horizontal: '1', vertical: '2',
                                  horizontal_inverted: '3',
                                  vertical_inverted: '4' }
      ruby_to_rust_directions[@direction]
    end

    def rust_formatted_detection_type
      ruby_to_rust_detection_type = { lightness_range: '0', colors: '1' }
      ruby_to_rust_detection_type[@detection_type]
    end

    def rust_formatted_sorting_by
      ruby_to_rust_sorting_by = { hue: '0', saturation: '1' }
      ruby_to_rust_sorting_by[@sorting_by]
    end
  end
end
