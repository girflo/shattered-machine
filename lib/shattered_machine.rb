# frozen_string_literal: true

require 'shattered_machine/converter'
require 'shattered_machine/glitcher'
require 'shattered_machine/io'
require 'shattered_machine/progressive_pixel_sorter'
require 'shattered_machine/sampler'

# main file for ShatteredMachine gem
module ShatteredMachine
  VERSION = '0.1.1'

  class << self
  end
end
