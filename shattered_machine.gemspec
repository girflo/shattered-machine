# frozen_string_literal: true

require 'English'

Gem::Specification.new do |s|
  s.name          = 'shattered_machine'
  s.version       = '0.1.2'
  s.required_ruby_version     = '>= 2.0.0'
  s.required_rubygems_version = '>= 1.8.11'
  s.summary       = 'Shattered Machine core engine'
  s.description   = 'Shattered Machine is an easy way to glitch PNG images using different algorithms'
  s.authors       = ['Flo Girardo']
  s.email         = 'florian@barbrousse.net'
  s.files = `git ls-files`.split($RS).reject do |file|
    file =~ %r{^spec/}
  end
  s.test_files    = ['spec']
  s.require_paths = ['lib']
  s.homepage      =
    'https://gitlab.com/girflo/shattered-machine'
  s.license       = 'MIT'
  s.add_dependency 'pnglitch', '~> 0.0.5'
  s.add_dependency 'rusty_engine_ffi', '~> 0.1.0'
  s.add_development_dependency('rspec', '~> 3')
  s.add_development_dependency('yard', '~> 0.9')
end
